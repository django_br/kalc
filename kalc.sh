#!/usr/bin/env bash

# Header --------------------------------
#[/]Ao receber expressões por pipe ou argumentos, não inicia o REPL e imprime a avaliação.
#[X]Expressões passadas como um único argumento.
#[X]Na forma de argumento, o valor de fix deve ser precedido pela opção -f.
#[X]Comandos externos permitidos: apenas o awk.
#[X]Todas as rotinas devem ser implementadas como funções.
#[X]Mensagens de erro significativas e definidas em vetores.
#[X]Opções obrigatórias: -h (ajuda) e -v (versão).
#[X]Projeto apresentado em repositório git próprio com licença e README.

# Texts ---------------------------------
shopt -s extglob 

versao="0.1.1"
ajuda="Calculador REPL do desafio final CIPB

Opções:

    -h|--help)           Mostra ajuda
    -v|--version)        Mostra a versao corrente
    -f)                  Informar a quantidade de casas decimais
"
fix=2;
exp=1;
var=1;

# Msgs de erro e uso --------------------
msg[1]='Apenas uma expressão como argumento'

# Funções -------------------------------
_die() {
    echo ${msg[$1]}
    exit $1
}

_opts(){
    case "$var" in
        -h|--help)      echo "$ajuda"; exit;;
        -v|--version)   echo "Version: $versao"; exit;;
        -f)             fix=$fix; exit;;
         *)             exp=$(tr -d ' ' <<< $exp); _kalc; exit;;
         '')            _main; exit;;
    esac
}

_kalc(){
awk -M 'BEGIN { printf "%.'$fix'f\n", ('"$exp"') }'
}

_main(){
    while :; do
        read -erp': ' exp
        [[ $exp = "q" ]] && echo bye && exit 0
        if [[ $exp = "-f" ]]; then
            read -erp 'Casas Decimais: ' fix
            read -erp': ' exp
        fi
        _kalc
    done
}

# Main ----------------------------------
if [[ -n $3 ]]; then
    _die 1
elif [[ -n $1 ]]; then 
    var="$1";fix="$2";exp="$@"
    _opts
else
    _main
fi
